import React, {useContext, useEffect} from 'react';
import {ActivityIndicator, Dimensions, ScrollView, View} from 'react-native';
import {useSafeAreaInsets} from 'react-native-safe-area-context';

import Carousel from 'react-native-snap-carousel';
import {GradientBackground} from '../components/GradientBackground';
import {HorizontalSlider} from '../components/HorizontalSlider';

import {MoviePoster} from '../components/MoviePoster';
import {useMovies} from '../hooks/useMovies';
import {getImageColors} from '../helpers/getImageColors';
import {GradientContext} from '../context/GradientContext';

// Obtener tamaño del celular
const {width: windowWidth} = Dimensions.get('window');

export const HomeScreen = () => {
  const {nowPlaying, popular, topRated, upcoming, isLoading} = useMovies();

  const {top} = useSafeAreaInsets();

  const {setMainColors} = useContext(GradientContext);

  const getPosterColors = async (index: number) => {
    const movie = nowPlaying[index];
    const uri = `https://image.tmdb.org/t/p/w500${movie.poster_path}`;

    const [primary = 'green', secondary = 'purple'] = await getImageColors(uri);

    setMainColors({primary, secondary});
  };

  useEffect(() => {
    if (nowPlaying.length > 0) {
      getPosterColors(0);
    }
  }, [nowPlaying]);

  if (isLoading) {
    return (
      <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
        <ActivityIndicator color="purple" size={100} />
      </View>
    );
  }

  // console.log(windowWidth);

  return (
    <GradientBackground>
      <ScrollView>
        <View style={{marginTop: top + 20}}>
          {/* <MoviePoster movie={moviesInCinema[1]} /> */}

          {/* Carousel principal */}
          <View style={{height: 390}}>
            <Carousel
              data={nowPlaying}
              renderItem={({item}: any) => <MoviePoster movie={item} />}
              sliderWidth={windowWidth}
              itemWidth={250}
              onSnapToItem={index => getPosterColors(index)} // Cuando el item tiene el foco
            />
          </View>

          {/* Carousel de peliculas populares */}
          <HorizontalSlider title="Populares" movies={popular} />
          {/* Carousel de peliculas populares */}
          <HorizontalSlider title="Los más valorados" movies={topRated} />
          {/* Carousel de peliculas populares */}
          <HorizontalSlider title="Próximos estrenos" movies={upcoming} />
        </View>
      </ScrollView>
    </GradientBackground>
  );
};
