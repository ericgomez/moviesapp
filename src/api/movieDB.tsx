import axios from 'axios';

const movieDB = axios.create({
  baseURL: 'https://api.themoviedb.org/3/movie',
  params: {
    api_key: '8e4ecdb0f5966248cd21c6f33f5464db',
    language: 'es-ES',
  },
});

export default movieDB;
